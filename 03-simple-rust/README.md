> Ref https://wasmbyexample.dev/examples/wasi-hello-world/wasi-hello-world.rust.en-us.html
```bash
cargo new --bin wasi_hello_world
rustup target add wasm32-wasi
cd wasi_hello_world
cargo build --target wasm32-wasi

cp target/wasm32-wasi/debug/wasi_hello_world.wasm ./wasi_hello_world.wasm 

wasmer wasi_hello_world.wasm
wasmtime wasi_hello_world.wasm 
```